#! /bin/bash

yum update install -y && yum install -y wget && yum install -y curl
groupadd -g 505 myusers && useradd -g 505 -u 505 anton_antanovich
groupadd -g 600 staff && useradd -g 600 -u 600 mongo && sudo passwd -d mongo

cd / && mkdir apps && mkdir /apps/mongo && mkdir /apps/mongodb

cd apps/ && chmod 750 mongo && chmod 750 mongodb

chown -R mongo:staff /apps/mongo/ && chown -R mongo:staff /apps/mongodb/

cd / && mkdir -p /logs/mongo && cd logs/ && chmod 750 mongo && chown -R mongo:staff mongo
echo _____1_____
su - mongo
cd /home/mongo/
wget https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-3.6.5.tgz
echo ____wget_____
curl https://fastdl.mongodb.org/src/mongodb-src-r3.6.5.tar.gz --output mongodb-src-r3.6.5.tar.gz
echo ___curl____
tar -xf mongodb-src-r3.6.5.tar.gz -C .
cp -r ./mongodb-src-r3.6.5/rpm/mongod.conf /apps/mongodb/
tar -C /tmp/ -xvf mongodb-linux-x86_64-3.6.5.tgz && cd /tmp/
cp -r ./mongodb-linux-x86_64-3.6.5/* /apps/mongo/
export PATH="/apps/mongo/bin:$PATH"
echo "PATH=/apps/mongo/bin/:$PATH" >> ~/.bashrc
echo "PATH=/apps/mongo/bin:$PATH" >> ~/.bash_profile
echo _____path______
su - root

echo "mongo      soft	nproc   32000" >> /etc/security/limits.d/20-nproc.conf
echo "mongo	 hard   nproc	32000" >> /etc/security/limits.d/20-nproc.conf

echo "anton_antanovich ALL=(mongo:staff) mongod" >> /etc/sudoers
cd /apps/mongodb/
sed -i 's/var\/log\/mongodb/logs\/mongo/g' ./mongod.conf
sed -i 's/var\/lib\/mongo/apps\/mongodb/g' ./mongod.conf
sed -i 's/var\/run\/mongodb/apps\/mongodb/g' ./mongod.conf
sed -i 's/fork: true/fork: false/g' ./mongod.conf
echo ____config______
echo "[Unit]
Description=mongo.service

ConditionPathExists=/apps/mongodb
ConditionPathExists=/apps/mongo/bin/mongod
ConditionPathExists=/logs/mongo

[Service]
User=mongo
Group=staff
ExecStart=/apps/mongo/bin/mongod -f /apps/mongodb/mongod.conf
ExecStartPre=/usr/bin/chown mongo:staff /apps/mongodb
ExecStartPre=/usr/bin/chown mongo:staff /logs/mongo
ExecStartPre=/usr/bin/chmod 0750 /apps/mongodb/
ExecStartPre=/usr/bin/chmod 0750 /logs/mongo/
PermissionsStartOnly=true

[Install]
WantedBy=multi-user.target
" >> /etc/systemd/system/mongod.service
echo ____unit___
systemctl daemon-reload
systemctl start mongod
systemctl status mongod
